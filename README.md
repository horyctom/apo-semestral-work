# Snake(s)

Semestral project for computer architectures course.
Implementation of snake game on hardware simulated via QtMips software.

## Getting Started

The project can only be run on QtMips simulator.

### Prerequisites

To succesfully compile the project, mips-elf compiler must be installed. In case of missing proper compiling tools, simple raw binary is provided.


### Installing

```
make 
```
MakeFile is provided and simple make command will do the job.

### Requirements
List of technical requirements to implement:

* RGB LED output
* LCD (framebuffer) outpit
* SERIAL PORT output
* Adjustable KNOBS INPUT

## Authors

* **Tomáš Horych** 

* **Kristýna Kořenská** 



