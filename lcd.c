/**
 * Utilities for direct lcd output 
 * 
**/


#include <stdint.h>
#include "font_types.h"

#define LCD_FB_START 0xffe00000
#define LCD_W 480
#define LCD_H 320

volatile uint16_t *disp = (volatile uint16_t *)LCD_FB_START;

uint16_t cur_x, cur_y;
uint16_t bg_color; // variables for position, background color and color of text
uint16_t fg_color;

void set_fg(uint16_t fg)
{
    fg_color = fg;
}

void set_bg(uint16_t bg)
{
    bg_color = bg;
}

uint16_t get_bg()
{
    return bg_color;
}

//  function for printing one character at cur_x, cur_y text position
void lcd_putc(uint8_t ch)
{
    uint16_t chpos, fontrow;
    uint16_t mask; //variable for bit mask with which we will compare every line
    long pixpos;

    chpos = ch * 16; // character with value from ascii * 16 (bcs font height) . This way we can find bitmap for our selected character
    for (int i = 0; i < 16; i++)
    {                                           // looping for every line
        fontrow = font_rom8x16.bits[chpos + i]; // fetch line of character generator from font_rom
        fontrow = fontrow >> 8;                 // shif bits and use just upper half - works for 8 pixel wide font
        for (int j = 0; j < 8; j++)
        {                                                                         // innner loop - scan each row
            mask = 1 << (7 - j);                                                  // set  1 on the end and shift for every loop
            pixpos = cur_x * 8 + 15 - (7 - j) + (cur_y * 16 * LCD_W) + LCD_W * i; // calculate the memory address for each pixel, 15 and 7 are there bcs we counting with 0
            if ((fontrow & mask) == 0)                                            // logical AND - when it is zero theres nothing to draw
            {
                disp[pixpos] = bg_color;
            }
            else
            {
                disp[pixpos] = fg_color; // otherwise, we will draw with our chosen color
            }
        }
    }
    cur_x++; // update the x position
    if (cur_x > LCD_W / 8)
    {
        cur_x = 0; // we are at the end of line ? If so, next line and collumn 0
        cur_y++;
    }
}

void lcd_puts(char *string, uint16_t x, uint16_t y)
{
    cur_x = x;
    cur_y = y;  
    char *p = string; // loop for reading whole string and calling function lcd_putc for every character in the string
    while (*p != '\0')
    {
        lcd_putc(*p);
        p++;
    }
}

void background_fill(){ //func for changing bg 
    for(int i = 0; i < LCD_W*LCD_H;i++){
    	disp[i] = bg_color;
    }
}
