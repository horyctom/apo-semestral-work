#ifndef __LCD_H__
#define __LCD_H__

#include <stdint.h>

void set_fg (uint16_t fg);
void set_bg (uint16_t bg);
uint16_t get_fg();
uint16_t get_bg();
void lcd_putc(uint8_t ch);
void lcd_puts(char *string,uint16_t x, uint16_t y);
void background_fill();

#endif
