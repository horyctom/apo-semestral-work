/**
 * Menu interaction logic ~ choosing color / bg color menu 
 * 
**/

#include "utils.h"
#include "menu.h"
#include "lcd.h"
#include "menu_draw.h"

#define SPILED_REG_BASE 0xffffc100
#define SERIAL_PORT_BASE 0xffffc000
#define SPILED_REG_LED_LINE_o 0x004
#define SPILED_REG_LED_RGB1_o 0x010
#define SPILED_REG_KNOBS_8BIT_o 0x024
#define SERP_RX_ST_REG_o 0x00
#define SERP_RX_ST_REG_READY_m 0x1
#define SERP_RX_DATA_REG_o 0x04


void choose_color(uint16_t *s_color){
	unsigned char *terminalBase = (unsigned char *)SERIAL_PORT_BASE;
	unsigned char *rgbBase = (unsigned char *)SPILED_REG_BASE;
	while (1){
		uint32_t color32bit = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o);
		*(volatile uint32_t *)(rgbBase + SPILED_REG_LED_RGB1_o) = color32bit;
		//any serial port action will break the loop - set the color (press any key to continue)
		if ((*(volatile uint32_t *)(terminalBase + SERP_RX_ST_REG_o) & SERP_RX_ST_REG_READY_m)){
			reduce_color_to_16bit(s_color,color32bit);
			return;
		}
	}
}

void bg_color_menu(char c){
	unsigned char *terminalBase = (unsigned char *)SERIAL_PORT_BASE;
	clear_main_menu();
	show_background_menu();
	while (1){ // loop for bg menu, until a,b,c,or d wont be selected , menu will be there
		if ((*(volatile uint32_t *)(terminalBase + SERP_RX_ST_REG_o) & SERP_RX_ST_REG_READY_m))
		{
			c = *(volatile uint32_t *)(terminalBase + SERP_RX_DATA_REG_o);
		}
		if (c == 'a')
		{ //if a is selected , bg changes color to dark blue , then we will return to our main menu (*i = 0)
			set_bg(0x1828);
			background_fill();
			break;
		}
		else if (c == 'b')
		{ //same for pink
			set_bg(0x800C);
			background_fill();
			//clear_background();
			break;
		}
		else if (c == 'c')
		{ //and for "sea foam"
			set_bg(0x1DF6);
			background_fill();
			//clear_background();
			break;
		}
		else if (c == 'd')
		{ //if d is selected, we will return to main menu immediately, color of bg will be same
			clear_background_menu();
			break;
		}
	}
	print_main_menu();
}


