#ifndef __MENU_UTILS_H__
#define __MENU_UTILS_H__

#include <stdint.h>

void print_main_menu();
void bg_color_menu(char c);
void clear_menu();
void background();
void clear_background();
void choose_color(uint16_t *s_color);
void print_end_game();

#endif