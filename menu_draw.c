#include "menu_draw.h"
#include "lcd.h"

void show_background_menu(){ //print background choices
	set_fg(0xffff);
	lcd_puts("Choose desired color",10,2);
	set_fg(0x1828);
	lcd_puts("a)Deep blue",10,4);
	set_fg(0x800C);
	lcd_puts("b)Pink",10,6);
	set_fg(0x1df6);
	lcd_puts("c)Sea foam",10,8);
	set_fg(0xffff);
	lcd_puts("d)return to main menu",10,10);
}

void clear_background_menu(){ // clear bg menu
	set_fg(get_bg());
	lcd_puts("Choose desired color",10,2);
	lcd_puts("1)Deep blue",10,4);
	lcd_puts("2)Pink",10,6);
	lcd_puts("3)Sea foam",10,8);
	lcd_puts("4)return to main menu",10,10);
}

void print_main_menu(){ 
	set_fg(0xffff);
	lcd_puts("MAIN MENU",10,2);
	lcd_puts("(1) Set snake 1 color",10,4);
	lcd_puts("(2) Set snake 2 color",10,6);
	lcd_puts("(3) Set background color",10,8);
	lcd_puts("(4) Start game ",10,10);
}

void clear_main_menu(){ // clear menu 
	set_fg(get_bg());
	lcd_puts("MAIN MENU",10,2);
	lcd_puts("(1) Set snake 1 color",10,4);
	lcd_puts("(2) Set snake 2 color",10,6);
	lcd_puts("(3) Set background color",10,8);
	lcd_puts("(4) Start game ",10,10);
}

void print_end_game(){
	set_fg(0xffff);
	lcd_puts("GAME OVER",24,9);
}