#ifndef __MENU_DRAW_H__
#define __MENU_DRAW_H__

void show_background_menu();
void clear_background_menu();
void print_main_menu();
void clear_main_menu();
void print_end_game();

#endif