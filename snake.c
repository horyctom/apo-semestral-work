#include <stdint.h>
#include "snake.h"
#include "utils.h"
#include "lcd.h"
#include "menu.h"
#include "menu_draw.h"

#define SPILED_REG_BASE 0xffffc100
#define SERIAL_PORT_BASE 0xffffc000

#define SPILED_REG_LED_LINE_o 0x004
#define SPILED_REG_LED_RGB1_o 0x010
#define SPILED_REG_LED_RGB2_o 0x014
#define SPILED_REG_LED_KBDWR_DIRECT_o 0x018

#define SPILED_REG_KNOBS_8BIT_o 0x024

#define SERP_RX_ST_REG_o 0x00
#define SERP_RX_ST_REG_READY_m 0x1

#define SERP_RX_DATA_REG_o 0x04

#define SERP_TX_ST_REG_o 0x08
#define SERP_TX_ST_REG_READY_m 0x1

#define SERP_TX_DATA_REG_o 0x0c

#define LCD_FB_START 0xffe00000
#define LCD_FB_END 0xffe4afff

#define LCD_W 480
#define LCD_H 320

#define GREEN 0x57e55b
#define RED 0xf91c1c

#define POS(x, y) (y * LCD_W + x) //index in frame buffer calculation
#define START_X1 50
#define START_Y1 50
#define START_X2 430
#define START_Y2 270

/* setup for QtMips */
asm(
	".global _start\n"
	"_start:\n"
	"   la  $gp,_gp\n"
	"   j   main\n");


//direction vectors
int vectors[4][2] = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
unsigned char *terminalBase = (unsigned char *)SERIAL_PORT_BASE;
unsigned char *rgbBase = (unsigned char *)SPILED_REG_BASE;
volatile uint16_t *lcd = (volatile uint16_t *)LCD_FB_START;

//write text onto the terminal
void print_to_terminal(char *str){
	for (int i = 0; str[i]; i++){
		// simple ready check
		while (!(*(volatile uint32_t *)(terminalBase + SERP_TX_ST_REG_o) & SERP_TX_ST_REG_READY_m));
		*(volatile uint32_t *)(terminalBase + SERP_TX_DATA_REG_o) = str[i];
	}
}

//sets snakes life condition from their position
void check_hit(snake *s1, snake *s2){
	if (lcd[POS(s1->pos[0], s1->pos[1])] != get_bg() ||
		is_out_of_bounds(s1->pos[0], s1->pos[1])){
		s1->alive = 0;
	}
	if (lcd[POS(s2->pos[0], s2->pos[1])] != get_bg() ||
		is_out_of_bounds(s2->pos[0], s2->pos[1])){
		s2->alive = 0;
	}
}

//refresh each LED diod appropriately to snakes condition
void draw_snake_condition(int s1Alive, int s2Alive){
	*(volatile uint32_t *)(rgbBase + SPILED_REG_LED_RGB1_o) = s1Alive ? GREEN : RED;
	*(volatile uint32_t *)(rgbBase + SPILED_REG_LED_RGB2_o) = s2Alive ? GREEN : RED;
}

//move snakes bodies, returns 0 if any dies
int move_snakes(snake *s1, snake *s2){
	/* draw snakes positions */
	lcd[POS(s1->pos[0], s1->pos[1])] = s1->color;
	lcd[POS(s2->pos[0], s2->pos[1])] = s2->color;

	/* update position to new one by adding direction vector */
	s1->pos[0] = s1->pos[0] + vectors[s1->vecIDX][0];
	s1->pos[1] = s1->pos[1] + vectors[s1->vecIDX][1];
	s2->pos[0] = s2->pos[0] + vectors[s2->vecIDX][0];
	s2->pos[1] = s2->pos[1] + vectors[s2->vecIDX][1];

	/* hits himself/opponent or border of the screen */
	check_hit(s1, s2);
	draw_snake_condition(s1->alive, s2->alive);
	return (s1->alive && s2->alive);
}


int main(){
	//random color init
	uint16_t snake1_color = 0xfaaf;
	uint16_t snake2_color = 0xffaf;
	uint32_t knob1_tmp, knob2_tmp, knob3_tmp;
	char c = ' '; //char to store menu choices into
	length = 0; //length of the snake (actually both snakes)

	//initial menu
	print_main_menu();
	while (1){
		if ((*(volatile uint32_t *)(terminalBase + SERP_RX_ST_REG_o) & SERP_RX_ST_REG_READY_m)){
			c = *(volatile uint32_t *)(terminalBase + SERP_RX_DATA_REG_o);
			if (c == '1'){
				print_to_terminal("Choose color for snake 1\n");
				choose_color(&snake1_color);
			}
			else if (c == '2'){
				print_to_terminal("Choose color for snake 2\n");
				choose_color(&snake2_color);
			}
			else if(c=='3'){
				bg_color_menu(c);
			}
			else if(c=='4'){
				clear_main_menu();
				break;
			}
		}
	}
	
	unsigned int sleep_delay = 100;
	uint32_t knob1 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0x00ff0000;
	uint32_t knob2 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0x0000ff00;
	uint32_t knob3 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0x000000ff;
	knob1 = knob1 >> 16;
	knob2 = knob2 >> 8;

	//snakes init
	snake s1 = {{START_X1, START_Y1}, 1, 1, snake1_color};
	snake s2 = {{START_X2, START_Y2}, 3, 1, snake2_color};

	//main game loop
	while (move_snakes(&s1, &s2))
	{
		// read character (if there is any) and change direction
		if ((*(volatile uint32_t *)(terminalBase + SERP_RX_ST_REG_o) & SERP_RX_ST_REG_READY_m)){
			c = *(volatile uint32_t *)(terminalBase + SERP_RX_DATA_REG_o);
			change_direction(&s1, &s2, c);
		}
		//save current knob val so we can check the difference
		knob1_tmp = knob1;
		knob2_tmp = knob2;
		knob3_tmp = knob3;
		knob1 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0xff0000;
		knob2 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0xff00;
		knob3 = *(volatile uint32_t *)(rgbBase + SPILED_REG_KNOBS_8BIT_o) & 0xff;

		//if value changed on knobs is significant enough (90+) change direction
		knob1 = knob1 >> 16;
		knob2 = knob2 >> 8;
		if (knob1_tmp/90 != knob1/90){
			knobs_action(knob1_tmp/90,knob1/90, &s1,NULL);
		}
		if (knob2_tmp/90 != knob2/90){
			knobs_action(knob2_tmp/90,knob2/90,NULL, &s2);
		}
		//knob3 changed => change speed
		if (knob3_tmp != knob3)	{
			change_speed(&sleep_delay, knob3);
		}
		my_sleep(sleep_delay);
		length++;
	}

	print_end_game();
	end_loop();
	return 0;
}
