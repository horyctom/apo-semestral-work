#ifndef __SNAKE_H__
#define __SNAKE_H__

#include <unistd.h>
#include <stdint.h>

unsigned int length;

/*pos = current position, direction = vector of direction */
typedef struct __attribute__((packed)){
	int pos[2];
	int vecIDX;
	int alive;
	uint16_t color;
}snake;


int move_snakes(snake *s1, snake *s2);
void print_to_terminal(char *str);
void draw_snake_condition(int s1,int s2);
void check_hit(snake *s1, snake *s2);



#endif
