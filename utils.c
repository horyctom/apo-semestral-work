/**
 * utilities that don't require any access to peripherals
 * 
 **/

#include "utils.h"
#include "snake.h"

#define LCD_W 480
#define LCD_H 320


// end program
void end_loop(){
	while(1);
}

// check borders
int is_out_of_bounds(int x, int y){
	return (x > LCD_W || x < 0 || y > LCD_H || y < 0);
}

//makes processor waste time
void my_sleep(unsigned int n){
	for(int i = 0; i < n; i++){
		asm("nop");
	}
}

/** slightly modified algorithm for 24 -> 16 bit rgb color conversion
 * Thanks to opensource practical implementation of rgb bit reduction
 * 
 * @author of the algorithm and link to original repository:
 *  https://github.com/dynphys/rgb-converter
 **/
void reduce_color_to_16bit(uint16_t *color16,uint32_t color32){
		uint16_t RGB565 = 0;
		uint32_t r = (color32 & 0x00ff0000) >> 16;
		uint32_t g = (color32 & 0xff00) >> 8;
		uint32_t b = color32 & 0xff;
		r = (r * 249 + 1014) >> 11;
		g = (g * 253 + 505) >> 10;
		b = (b * 249 + 1014) >> 11;
		RGB565 = RGB565 | (r << 11);
		RGB565 = RGB565 | (g << 5);
		RGB565 = RGB565 | b;
		*color16 = RGB565;
}

// sets speed according to the position of the knob
void change_speed(unsigned int *delay, int knob_pos){
	if(knob_pos < 52 && knob_pos >= 0){
		(*delay) = 400;
	}else if(knob_pos < 102 && knob_pos > 51){
		(*delay) = 200;
	}else if(knob_pos < 152 && knob_pos > 101){
		(*delay) = 100;
	}else if(knob_pos < 202 && knob_pos > 151){
		(*delay) = 50;
	}else{
		(*delay) = 0;
	}
}

//according to knobs change make the snake direction change
void knobs_action(uint32_t knob_tmp, uint32_t knob, snake *s1, snake *s2){
	//deciding which snake we will take care about
	char left = s1 == NULL ? 'k' : 'a';
	char right = s1 == NULL ? 'l' : 's';
	//0-255 special case
	if((knob_tmp == 2) && (knob == 0)){
		change_direction(s1,s2,right);
	}else if((knob_tmp == 0) && (knob == 2)){
		change_direction(s1,s2,left);
	}
	else if(knob > knob_tmp){
		change_direction(s1,s2,right);
	}
	else if(knob < knob_tmp){
		change_direction(s1,s2,left);
	}
}

// modifies direction-vector index of each snake
void change_direction(snake *s1, snake *s2, char c)
{
	//s1 LEFT
	if (c == 'a'){
		s1->vecIDX = (s1->vecIDX - 1 + 4) % 4;
		//s1 RIGHT
	}else if (c == 's'){
		s1->vecIDX = (s1->vecIDX + 1 + 4) % 4;
		//s2 LEFT
	}else if (c == 'k'){
		s2->vecIDX = (s2->vecIDX - 1 + 4) % 4;
		//s2 RIGHT
	}else if (c == 'l'){
		s2->vecIDX = (s2->vecIDX + 1 + 4) % 4;
	}
}



