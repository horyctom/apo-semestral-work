#ifndef __UTILS_H__
#define __UTILS_H__

#include "snake.h"

int is_out_of_bounds(int x, int y);
void my_sleep(unsigned int n);
void end_loop();
void change_direction(snake *s1, snake *s2, char c);
void change_speed(unsigned int *delay, int knob_pos);
void reduce_color_to_16bit(uint16_t *color16,uint32_t color32);
void knobs_action(uint32_t knob_tmp, uint32_t knob, snake *s1, snake *s2);

#endif
